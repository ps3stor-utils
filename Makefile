
CC=gcc
CFLAGS=-Wall -O2 -g -m64
LDFLAGS=-m64

SRC_ps3stor_region=ps3stor_mgr.c ps3stor_region.c
OBJ_ps3stor_region=$(SRC_ps3stor_region:.c=.o)
TARGET_ps3stor_region=ps3stor_region

all: $(TARGET_ps3stor_region)

$(TARGET_ps3stor_region): $(OBJ_ps3stor_region)
	$(CC) $(LDFLAGS) -o $@ $^

%.o: %.c
	$(CC) $(CFLAGS) -c $<

.PHONY: clean
clean:
	rm -f $(OBJ_ps3stor_region) $(TARGET_ps3stor_region)
