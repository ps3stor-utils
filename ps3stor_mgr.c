
/*
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 2 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */

#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <sys/ioctl.h>
#include <unistd.h>
#include <asm/ps3stormgr.h>

#include "ps3stor_mgr.h"

int ps3stor_mgr_open(const char *path)
{
	return open(path, O_RDWR);
}

int ps3stor_mgr_close(int fd)
{
	return close(fd);
}

int ps3stor_mgr_create_region(int fd, uint64_t dev_id, uint64_t start_sector,
	uint64_t sector_count, uint64_t laid, uint64_t *region_id)
{
	struct ps3stormgr_ioctl_create_region arg;
	int error;

	memset(&arg, 0, sizeof(arg));
	arg.dev_id = dev_id;
	arg.start_sector = start_sector;
	arg.sector_count = sector_count;
	arg.laid = laid;

	error = ioctl(fd, PS3STORMGR_IOCTL_CREATE_REGION, &arg);
	
	if (!error)
		*region_id = arg.region_id;

	return error;
}

int ps3stor_mgr_delete_region(int fd, uint64_t dev_id, uint64_t region_id)
{
	struct ps3stormgr_ioctl_delete_region arg;

	memset(&arg, 0, sizeof(arg));
	arg.dev_id = dev_id;
	arg.region_id = region_id;

	return ioctl(fd, PS3STORMGR_IOCTL_DELETE_REGION, &arg);
}

int ps3stor_mgr_set_region_acl(int fd, uint64_t dev_id, uint64_t region_id,
	uint64_t laid, uint64_t access_rights)
{
	struct ps3stormgr_ioctl_set_region_acl arg;

	memset(&arg, 0, sizeof(arg));
	arg.dev_id = dev_id;
	arg.region_id = region_id;
	arg.laid = laid;
	arg.access_rights = access_rights;

	return ioctl(fd, PS3STORMGR_IOCTL_SET_REGION_ACL, &arg);
}

int ps3stor_mgr_get_region_acl(int fd, uint64_t dev_id, uint64_t region_id,
	uint64_t entry_index, uint64_t *laid, uint64_t *access_rights)
{
	struct ps3stormgr_ioctl_get_region_acl arg;
	int error;

	memset(&arg, 0, sizeof(arg));
	arg.dev_id = dev_id;
	arg.region_id = region_id;
	arg.entry_index = entry_index;

	error = ioctl(fd, PS3STORMGR_IOCTL_GET_REGION_ACL, &arg);

	if (!error) {
		*laid = arg.laid;
		*access_rights = arg.access_rights;
	}

	return error;
}
