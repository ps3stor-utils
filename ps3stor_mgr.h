
/*
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 2 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */

#ifndef _PS3STOR_MGR_H_
#define _PS3STOR_MGR_H_

#include <stdint.h>

int ps3stor_mgr_open(const char *path);

int ps3stor_mgr_close(int fd);

int ps3stor_mgr_create_region(int fd, uint64_t dev_id, uint64_t start_sector,
	uint64_t sector_count, uint64_t laid, uint64_t *region_id);

int ps3stor_mgr_delete_region(int fd, uint64_t dev_id, uint64_t region_id);

int ps3stor_mgr_set_region_acl(int fd, uint64_t dev_id, uint64_t region_id,
	uint64_t laid, uint64_t access_rights);

int ps3stor_mgr_get_region_acl(int fd, uint64_t dev_id, uint64_t region_id,
	uint64_t entry_index, uint64_t *laid, uint64_t *access_rights);

#endif
