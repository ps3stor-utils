
/*
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 2 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <getopt.h>
#include <errno.h>

#include "ps3stor_mgr.h"

#define PS3STOR_REGION_VERSION		"0.0.1"

struct opts
{
	char *device_name;
	char *cmd;
	int do_help;
	int do_verbose;
	int do_version;
};

static struct option long_opts[] = {
	{ "help",	no_argument, NULL, 'h' },
	{ "verbose",	no_argument, NULL, 'v' },
	{ "version",	no_argument, NULL, 'V' },
	{ NULL, 0, NULL, 0 }
};

/*
 * usage
 */
static void usage(void) {
	fprintf(stderr,
		"Usage: ps3stor_region [OPTIONS] DEVICE COMMAND [ARGS]\n"
		"\n"
		"Options:\n"
		"	-h, --help				Show this message and exit\n"
		"	-v, --verbose				Increase verbosity\n"
		"	-V, --version				Show version information and exit\n"
		"Commands:\n"
		"	create DEVID START COUNT LAID		Creates storage region\n"
		"	delete DEVID REGIONID			Deletes storage region\n"
		"	set_acl DEVID REGIONID LAID RIGHTS	Sets region access rights\n"
		"	get_acl DEVID REGIONID INDEX		Returns region access rights\n"
		"\n\n"
		"Simple example: Create a HDD region:\n"
		"	ps3stor_region /dev/ps3stormgr create 3 0x8 0x1000 0x1070000002000001\n");
}

/*
 * version
 */
static void version(void)
{
	fprintf(stderr,
		"ps3stor_region " PS3STOR_REGION_VERSION "\n"
		"Copyright (C) 2011 graf_chokolo <grafchokolo@googlemail.com>\n"
		"This is free software.  You may redistribute copies of it "
		"under the terms of\n"
		"the GNU General Public License 2 "
		"<http://www.gnu.org/licenses/gpl2.html>.\n"
		"There is NO WARRANTY, to the extent permitted by law.\n");
}

/*
 * process_opts
 */
static int process_opts(int argc, char **argv, struct opts *opts)
{
	int c;

	while ((c = getopt_long(argc, argv, "hvV", long_opts, NULL)) != -1) {
		switch (c) {
		case 'h':
		case '?':
			opts->do_help = 1;
			return 0;

		case 'v':
			opts->do_verbose++;
			break;

		case 'V':
			opts->do_version = 1;
			return 0;

		default:
			fprintf(stderr, "Invalid command option: %c\n", c);
			return -1;
		}
	}

	if (optind >= argc) {
		fprintf(stderr, "No device specified\n");
		return -1;
	}

	opts->device_name = argv[optind];
	optind++;

	if (optind >= argc) {
		fprintf(stderr, "No command specified\n");
		return -1;
	}

	opts->cmd = argv[optind];
	optind++;

	return 0;
}

/*
 * cmd_create
 */
static int cmd_create(int fd, struct opts *opts, int argc, char **argv)
{
	uint64_t dev_id, start_sector, sector_count, laid, region_id;
	char *endptr;
	int error;

	if (optind >= argc) {
		fprintf(stderr, "No device identifier specified\n");
		return -1;
	}

	dev_id = strtoull(argv[optind], &endptr, 0);
	if (*endptr != '\0') {
		fprintf(stderr, "Invalid device identifier specified\n");
		return -1;
	}

	optind++;

	if (optind >= argc) {
		fprintf(stderr, "No start sector specified\n");
		return -1;
	}

	start_sector = strtoull(argv[optind], &endptr, 0);
	if (*endptr != '\0') {
		fprintf(stderr, "Invalid start sector specified\n");
		return -1;
	}

	optind++;

	if (optind >= argc) {
		fprintf(stderr, "No sector count specified\n");
		return -1;
	}

	sector_count = strtoull(argv[optind], &endptr, 0);
	if (*endptr != '\0') {
		fprintf(stderr, "Invalid sector count specified\n");
		return -1;
	}

	optind++;

	if (optind >= argc) {
		fprintf(stderr, "No LPAR authentication identifier specified\n");
		return -1;
	}

	laid = strtoull(argv[optind], &endptr, 0);
	if (*endptr != '\0') {
		fprintf(stderr, "Invalid LPAR authentication identifier specified\n");
		return -1;
	}

	optind++;

	error = ps3stor_mgr_create_region(fd, dev_id, start_sector, sector_count, laid,
		&region_id);

	if (error)
		fprintf(stderr, "%s: %s\n", opts->device_name, strerror(errno));
	else
		fprintf(stdout, "0x%016lx\n", region_id);

	return error;
}

/*
 * cmd_delete
 */
static int cmd_delete(int fd, struct opts *opts, int argc, char **argv)
{
	uint64_t dev_id, region_id;
	char *endptr;
	int error;

	if (optind >= argc) {
		fprintf(stderr, "No device identifier specified\n");
		return -1;
	}

	dev_id = strtoull(argv[optind], &endptr, 0);
	if (*endptr != '\0') {
		fprintf(stderr, "Invalid device identifier specified\n");
		return -1;
	}

	optind++;

	if (optind >= argc) {
		fprintf(stderr, "No region identifier specified\n");
		return -1;
	}

	region_id = strtoull(argv[optind], &endptr, 0);
	if (*endptr != '\0') {
		fprintf(stderr, "Invalid region identifier specified\n");
		return -1;
	}

	optind++;

	error = ps3stor_mgr_delete_region(fd, dev_id, region_id);

	if (error)
		fprintf(stderr, "%s: %s\n", opts->device_name, strerror(errno));

	return error;
}

/*
 * cmd_set_acl
 */
static int cmd_set_acl(int fd, struct opts *opts, int argc, char **argv)
{
	uint64_t dev_id, region_id, laid, access_rights;
	char *endptr;
	int error;

	if (optind >= argc) {
		fprintf(stderr, "No device identifier specified\n");
		return -1;
	}

	dev_id = strtoull(argv[optind], &endptr, 0);
	if (*endptr != '\0') {
		fprintf(stderr, "Invalid device identifier specified\n");
		return -1;
	}

	optind++;

	if (optind >= argc) {
		fprintf(stderr, "No region identifier specified\n");
		return -1;
	}

	region_id = strtoull(argv[optind], &endptr, 0);
	if (*endptr != '\0') {
		fprintf(stderr, "Invalid region identifier specified\n");
		return -1;
	}

	optind++;

	if (optind >= argc) {
		fprintf(stderr, "No LPAR authentication identifier specified\n");
		return -1;
	}

	laid = strtoull(argv[optind], &endptr, 0);
	if (*endptr != '\0') {
		fprintf(stderr, "Invalid LPAR authentication identifier specified\n");
		return -1;
	}

	optind++;

	if (optind >= argc) {
		fprintf(stderr, "No access rights specified\n");
		return -1;
	}

	access_rights = strtoull(argv[optind], &endptr, 0);
	if (*endptr != '\0') {
		fprintf(stderr, "Invalid access rights specified\n");
		return -1;
	}

	optind++;

	error = ps3stor_mgr_set_region_acl(fd, dev_id, region_id, laid, access_rights);

	if (error)
		fprintf(stderr, "%s: %s\n", opts->device_name, strerror(errno));

	return error;
}

/*
 * cmd_get_acl
 */
static int cmd_get_acl(int fd, struct opts *opts, int argc, char **argv)
{
	uint64_t dev_id, region_id, entry_index, laid, access_rights;
	char *endptr;
	int error;

	if (optind >= argc) {
		fprintf(stderr, "No device identifier specified\n");
		return -1;
	}

	dev_id = strtoull(argv[optind], &endptr, 0);
	if (*endptr != '\0') {
		fprintf(stderr, "Invalid device identifier specified\n");
		return -1;
	}

	optind++;

	if (optind >= argc) {
		fprintf(stderr, "No region identifier specified\n");
		return -1;
	}

	region_id = strtoull(argv[optind], &endptr, 0);
	if (*endptr != '\0') {
		fprintf(stderr, "Invalid region identifier specified\n");
		return -1;
	}

	optind++;

	if (optind >= argc) {
		fprintf(stderr, "No entry index specified\n");
		return -1;
	}

	entry_index = strtoull(argv[optind], &endptr, 0);
	if (*endptr != '\0') {
		fprintf(stderr, "Invalid entry index specified\n");
		return -1;
	}

	optind++;

	error = ps3stor_mgr_get_region_acl(fd, dev_id, region_id, entry_index, &laid, &access_rights);

	if (error)
		fprintf(stderr, "%s: %s\n", opts->device_name, strerror(errno));
	else
		fprintf(stdout, "0x%016lx 0x%016lx\n", laid, access_rights);

	return error;
}

/*
 * main
 */
int main(int argc, char **argv)
{
	struct opts opts;
	int fd = 0, error = 0;

	memset(&opts, 0, sizeof(opts));

	if (process_opts(argc, argv, &opts)) {
		usage();
		error = 1;
		goto done;
	}

	if (opts.do_help) {
		usage();
		goto done;
	} else if (opts.do_version) {
		version();
		goto done;
	}

	fd = ps3stor_mgr_open(opts.device_name);
	if (fd < 0) {
		fprintf(stderr, "%s: %s\n", opts.device_name, strerror(errno));
		error = 2;
		goto done;
	}

	if (!strcmp(opts.cmd, "create")) {
		error = cmd_create(fd, &opts, argc, argv);
	} else if (!strcmp(opts.cmd, "delete")) {
		error = cmd_delete(fd, &opts, argc, argv);
	} else if (!strcmp(opts.cmd, "set_acl")) {
		error = cmd_set_acl(fd, &opts, argc, argv);
	} else if (!strcmp(opts.cmd, "get_acl")) {
		error = cmd_get_acl(fd, &opts, argc, argv);
	} else {
		usage();
		error = 1;
		goto done;
	}

	if (error)
		error = 3;

done:

	if (fd >= 0)
		ps3stor_mgr_close(fd);

	exit(error);
}
